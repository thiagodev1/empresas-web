import React, { Fragment, useState, useContext, useEffect } from "react";
import AuthContext from "../../context/auth/authContext";
import { useHistory } from "react-router-dom";
import { Spinner } from "../layout/Spinner";

const Login = () => {
  const authContext = useContext(AuthContext);
  const { login, isAuthenticated, loading } = authContext;

  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const { password, email } = user;

  let history = useHistory();

  useEffect(() => {
    if (isAuthenticated) {
      history.push("/home");
    }
  }, [history, isAuthenticated]);

  const onChange = (e) => setUser({ ...user, [e.target.name]: e.target.value });

  const onSubmit = (e) => {
    e.preventDefault();
    login({
      email,
      password,
    });
  };

  return (
    <Fragment>
      {loading && <Spinner />}
      <div
        className='container'
        style={{ textAlign: "center", marginTop: "50px" }}
      >
        <img
          className='logo_home'
          src='https://cdn.zeplin.io/5977c41d78a49a0f1bf3966b/assets/231F3628-3A89-44CF-904A-3895F0A5E89F.png'
          alt=''
        />
        <p className='BEM-VINDO-AO-EMPRESA'>
          BEM-VINDO AO <br /> EMPRESAS
        </p>
        <p className='Lorem-ipsum-dolor-si'>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorum iste
          quo fuga porro doloribus
        </p>
        <div className='row'>
          <div className='col s3'></div>
          <div className='col s6'>
            <form onSubmit={onSubmit}>
              <div className='row'>
                <div className='input-field col s12'>
                  <input
                    required
                    id='input_email'
                    type='email'
                    name='email'
                    value={email}
                    onChange={onChange}
                    placeholder='email'
                    data-length='10'
                  />
                </div>
              </div>
              <div className='row'>
                <div className='input-field col s12'>
                  <input
                    required
                    id='input_password'
                    type='password'
                    name='password'
                    value={password}
                    onChange={onChange}
                    placeholder='senha'
                    data-length='10'
                  />
                </div>
              </div>
              <button
                className='waves-effect waves-light btn-large block'
                type='submit'
                style={{ display: "block", width: "100%" }}
              >
                Button
              </button>
            </form>
          </div>
          <div className='col s3'></div>
        </div>
      </div>
    </Fragment>
  );
};

export default Login;
