import React, { Fragment } from "react";

const Pagination = ({ postsPerPage, totalPosts, paginate }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <Fragment>
      <ul className='pagination' style={{ textAlign: "center" }}>
        {pageNumbers.map((number) => (
          <li key={number} className='waves-effect'>
            <button
              className='waves-effect waves-teal btn-flat'
              onClick={() => paginate(number)}
            >
              {number}
            </button>
          </li>
        ))}
      </ul>
    </Fragment>
  );
};

export default Pagination;
