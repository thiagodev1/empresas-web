import React, { useContext, Fragment } from "react";
import EnterpriseContext from "../../context/enterprise/enterpriseContext";
import { useHistory } from "react-router-dom";
import { Spinner } from "../layout/Spinner";

const Home = () => {
  const enterpriseContext = useContext(EnterpriseContext);
  const { enterprisesFilter, loading, cleanSearchResult } = enterpriseContext;
  let history = useHistory();

  return (
    <Fragment>
      {loading && <Spinner />}

      <div className='container'>
        <div className='row'>
          <div className='col s12'>
            {" "}
            <button
              className='waves-effect waves-light btn-small block center-align'
              onClick={() => history.push(`/allEnterprises`)}
              style={{ width: "100%", marginTop: "20px", marginBottom: "20px" }}
            >
              Listar todas as empresas
            </button>
            <button
              className='waves-effect waves-light btn-small block center-align'
              style={{ width: "100%", marginTop: "5px", marginBottom: "15px" }}
              onClick={() => cleanSearchResult()}
            >
              Limpar Pesquisa
            </button>
            {enterprisesFilter.length === 0 && (
              <h3 style={{ color: "#b5b4b4", textAlign: "center" }}>
                Nenhuma empresa foi econtrada para a busca realizada.
              </h3>
            )}
            {enterprisesFilter &&
              enterprisesFilter.map((enterprise) => (
                <div key={enterprise.id} className='card horizontal'>
                  <div className='card-stacked'>
                    <div className='card-content'>
                      <div className='row'>
                        <div className='col s6'>
                          <img
                            className='responsive-img'
                            src={`https://empresas.ioasys.com.br${enterprise.photo}`}
                            alt=''
                          />
                        </div>
                        <div className='col s6'>
                          <h5>{enterprise.enterprise_name}</h5>
                          <p style={{ marginBottom: "5px" }}>
                            {enterprise.city}{" "}
                          </p>
                          <p>{enterprise.country} </p>
                          <div>
                            <button
                              className='waves-effect waves-light btn-small'
                              style={{ marginTop: "15px" }}
                              onClick={() =>
                                history.push(`/details/${enterprise.id}`)
                              }
                            >
                              Detalhes
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Home;
