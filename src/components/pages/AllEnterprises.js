import React, { useEffect, useContext, Fragment, useState } from "react";
import AuthContext from "../../context/auth/authContext";
import EnterpriseContext from "../../context/enterprise/enterpriseContext";
import { useHistory } from "react-router-dom";
import { Spinner } from "../layout/Spinner";
import Pagination from "../Pagination/Pagination";

const AllEnterprises = () => {
  const enterpriseContext = useContext(EnterpriseContext);
  const { showAll, enterprises, loading } = enterpriseContext;
  const authContext = useContext(AuthContext);
  const { accessToken, uid, client } = authContext;
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(6);
  let history = useHistory();

  useEffect(() => {
    const headers = { accessToken, client, uid };
    showAll(headers);
    // eslint-disable-next-line
  }, []);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentDocs = enterprises.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <Fragment>
      {loading && <Spinner />}
      <div className='container'>
        <div className='row'>
          <div className='col s12'>
            {currentDocs &&
              currentDocs.map((enterprise) => (
                <div
                  style={{ marginTop: "25px" }}
                  key={enterprise.id}
                  className='card horizontal'
                >
                  <div className='card-stacked'>
                    <div className='card-content'>
                      <div className='row'>
                        <div className='col s6'>
                          <img
                            className='responsive-img'
                            src={`https://empresas.ioasys.com.br${enterprise.photo}`}
                            alt=''
                          />
                        </div>
                        <div className='col s6'>
                          <h5>{enterprise.enterprise_name}</h5>
                          <p style={{ marginBottom: "5px" }}>
                            {enterprise.city}{" "}
                          </p>
                          <p>{enterprise.country} </p>
                          <div>
                            <button
                              className='waves-effect waves-light btn-small'
                              style={{ marginTop: "15px" }}
                              onClick={() =>
                                history.push(`/details/${enterprise.id}`)
                              }
                            >
                              detalhes
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            <Pagination
              postsPerPage={postsPerPage}
              totalPosts={enterprises.length}
              paginate={paginate}
            />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default AllEnterprises;
