import React, { useEffect, useContext } from "react";
import EnterpriseContext from "../../context/enterprise/enterpriseContext";
import AuthContext from "../../context/auth/authContext";
import { useParams } from "react-router-dom";
import { Spinner } from "../layout/Spinner";
import { useHistory } from "react-router-dom";

const Details = () => {
  const enterpriseContext = useContext(EnterpriseContext);
  const { showDetails, enterpriseDetails, loading } = enterpriseContext;
  const authContext = useContext(AuthContext);
  const { accessToken, uid, client } = authContext;
  let { id } = useParams();
  let history = useHistory();

  useEffect(() => {
    const headers = { accessToken, client, uid };
    showDetails(headers, id);
    // eslint-disable-next-line
  }, [id]);

  return (
    <div>
      {loading && <Spinner />}
      {enterpriseDetails && (
        <div className='container'>
          <div className='row'>
            <div className='col s12'>
              <button
                onClick={() => history.goBack()}
                className='waves-effect waves-light btn'
                style={{ marginTop: "15px" }}
              >
                <i className='material-icons left'>arrow_back</i>Voltar
              </button>
              <div style={{ marginTop: "25px" }} className='card horizontal'>
                <div className='card-stacked'>
                  <div className='card-content'>
                    <div style={{ textAlign: "center" }}>
                      <img
                        className='responsive-img'
                        src={`https://empresas.ioasys.com.br${enterpriseDetails.photo}`}
                        alt=''
                      />
                      <div style={{ marginTop: "25px" }}>
                        {enterpriseDetails.description}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Details;
