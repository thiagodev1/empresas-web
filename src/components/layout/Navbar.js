import React, { useContext, useState } from "react";
import { DebounceInput } from "react-debounce-input";
import EnterpriseContext from "../../context/enterprise/enterpriseContext";
import AuthContext from "../../context/auth/authContext";

const Navbar = () => {
  const enterpriseContext = useContext(EnterpriseContext);
  const { filter, cleanSearchResult } = enterpriseContext;
  const authContext = useContext(AuthContext);
  const { accessToken, uid, client } = authContext;

  const [showInput, setShowInput] = useState(false);

  const enterprisesFilter = (text) => {
    if (text === "") {
      cleanSearchResult();
    } else {
      const headers = { accessToken, client, uid };
      filter(headers, text);
    }
  };

  return (
    <nav className='nav-color'>
      <div className='nav-wrapper'>
        {!showInput ? (
          <div className='container'>
            <div className='brand-logo center'>
              <img
                src='https://cdn.zeplin.io/5977c41d78a49a0f1bf3966b/assets/256B1238-AF76-4029-95C9-2CEC0AD6FC5D.png'
                width='150'
                height='auto'
                style={{ verticalAlign: "text-top" }}
                alt=''
              />
            </div>
            <ul className='right'>
              <li>
                <span style={{ cursor: "pointer" }}>
                  <i
                    onClick={() => setShowInput(!showInput)}
                    className='material-icons'
                  >
                    search
                  </i>
                </span>
              </li>
            </ul>
          </div>
        ) : (
          <div className='container'>
            <div className='row'>
              <div className='col s11'>
                {" "}
                <DebounceInput
                  className='cor'
                  minLength={3}
                  debounceTimeout={500}
                  onChange={(e) => enterprisesFilter(e.target.value)}
                />
              </div>
              <div className='col s1'>
                {" "}
                <ul className='left'>
                  <li>
                    <span style={{ cursor: "pointer" }}>
                      <i
                        onClick={() => setShowInput(!showInput)}
                        className='material-icons'
                      >
                        close
                      </i>
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
