import axios from "axios";

const setAuthToken = (token) => {
  axios.defaults.headers.common["access-token"] = token.accessToken;
  axios.defaults.headers.common["client"] = token.client;
  axios.defaults.headers.common["uid"] = token.uid;
};

export default setAuthToken;
