import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/auth/Login";
import AuthState from "./context/auth/AuthState";
import EnterpriseState from "./context/enterprise/EnterpriseState";
import "materialize-css/dist/css/materialize.min.css";
import Home from "../src/components/pages/Home";
import Details from "../src/components/pages/Details";
import PrivateRoute from "../src/components/routing/PrivateRoute";
import { Fragment } from "react";
import Navbar from "./components/layout/Navbar";
import AllEnterprises from "./components/pages/AllEnterprises";
import NotFound from "./components/pages/NotFound";

const App = () => {
  return (
    <AuthState>
      <EnterpriseState>
        <Router>
          <Route exact path='/' component={Login} />
          <Route
            path={"/(.+)"}
            render={() => (
              <Fragment>
                <Navbar />
                {}
                <Switch>
                  <PrivateRoute exact path='/home' component={Home} />
                  <PrivateRoute exact path='/details/:id' component={Details} />
                  <PrivateRoute
                    exact
                    path='/allEnterprises'
                    component={AllEnterprises}
                  />
                  <Route component={NotFound} />
                </Switch>
              </Fragment>
            )}
          />
        </Router>
      </EnterpriseState>
    </AuthState>
  );
};

export default App;
