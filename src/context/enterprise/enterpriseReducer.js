import {
  SHOW_ALL,
  FILTER,
  SHOW_DETAILS,
  FILTER_FAIL,
  SHOW_ALL_FAIL,
  SHOW_DETAILS_FAIL,
  SET_LOADING,
  CLEAN_SEARCH_RESULT,
  CLEAN_DETAILS_INFO,
} from "../types";

export const enterpriseReducer = (state, action) => {
  switch (action.type) {
    case SHOW_ALL:
      return {
        ...state,
        enterprises: action.payload,
        loading: false,
      };
    case FILTER:
      return {
        ...state,
        enterprisesFilter: action.payload,
        loading: false,
      };
    case FILTER_FAIL:
    case SHOW_ALL_FAIL:
    case SHOW_DETAILS_FAIL:
      return {
        ...state,
        enterprises: [],
        enterprisesFilter: [],
        enterpriseDetails: null,
        loading: false,
      };
    case CLEAN_SEARCH_RESULT:
      return {
        ...state,
        enterprisesFilter: [],
      };
    case SET_LOADING:
      return {
        ...state,
        loading: true,
      };
    case CLEAN_DETAILS_INFO:
      return {
        ...state,
        enterpriseDetails: undefined,
      };
    case SHOW_DETAILS:
      return {
        ...state,
        enterpriseDetails: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};
