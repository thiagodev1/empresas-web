import React, { useReducer } from "react";
import axios from "axios";
import EnterpriseContext from "./enterpriseContext";
import { enterpriseReducer } from "./enterpriseReducer";
import {
  SHOW_ALL,
  SHOW_DETAILS,
  FILTER,
  FILTER_FAIL,
  SHOW_DETAILS_FAIL,
  SHOW_ALL_FAIL,
  SET_LOADING,
  CLEAN_SEARCH_RESULT,
  CLEAN_DETAILS_INFO,
} from "../types";

import setAuthToken from "../../utils/setAuthToken";

const EnterpriseState = (props) => {
  const initialState = {
    enterprises: [],
    enterprisesFilter: [],
    enterpriseDetails: null,
    loading: false,
  };

  const showAll = async (authHeaders) => {
    setAuthToken(authHeaders);
    try {
      setLoading();
      const res = await axios.get(
        "https://empresas.ioasys.com.br/api/v1/enterprises"
      );
      dispatch({
        type: SHOW_ALL,
        payload: res.data.enterprises,
      });
    } catch (e) {
      dispatch({
        type: SHOW_ALL_FAIL,
      });
    }
  };

  const filter = async (authHeaders, inputValue) => {
    setAuthToken(authHeaders);
    try {
      setLoading();
      const res = await axios.get(
        `https://empresas.ioasys.com.br/api/v1/enterprises?name=${inputValue}`
      );
      dispatch({
        type: FILTER,
        payload: res.data.enterprises,
      });
    } catch (e) {
      dispatch({
        type: FILTER_FAIL,
      });
    }
  };

  const showDetails = async (authHeaders, id) => {
    setAuthToken(authHeaders);
    try {
      setLoading();
      cleanDetailsInfo();
      const res = await axios.get(
        `https://empresas.ioasys.com.br/api/v1/enterprises/${id}`
      );
      dispatch({
        type: SHOW_DETAILS,
        payload: res.data.enterprise,
      });
    } catch (e) {
      dispatch({
        type: SHOW_DETAILS_FAIL,
      });
    }
  };

  const setLoading = () => dispatch({ type: SET_LOADING });

  const cleanSearchResult = () => dispatch({ type: CLEAN_SEARCH_RESULT });

  const cleanDetailsInfo = () => dispatch({ type: CLEAN_DETAILS_INFO });

  const [state, dispatch] = useReducer(enterpriseReducer, initialState);

  return (
    <EnterpriseContext.Provider
      value={{
        showAll,
        filter,
        showDetails,
        cleanSearchResult,
        enterprises: state.enterprises,
        enterprisesFilter: state.enterprisesFilter,
        enterpriseDetails: state.enterpriseDetails,
        loading: state.loading,
      }}
    >
      {props.children}
    </EnterpriseContext.Provider>
  );
};

export default EnterpriseState;
