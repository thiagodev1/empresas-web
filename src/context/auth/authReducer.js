import { LOGIN_SUCCESS, LOGIN_FAIL, SET_LOADING } from "../types";

export const authReducer = (state, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        client: action.payload["client"],
        uid: action.payload["uid"],
        accessToken: action.payload["access-token"],
        isAuthenticated: true,
        loading: false,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        client: null,
        uid: null,
        accessToken: null,
        isAuthenticated: false,
        loading: false,
      };
    case SET_LOADING:
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
};
