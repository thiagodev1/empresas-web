import React, { useReducer } from "react";
import axios from "axios";
import AuthContext from "./authContext";
import { authReducer } from "./authReducer";
import { LOGIN_FAIL, LOGIN_SUCCESS, SET_LOADING } from "../types";

const AuthState = (props) => {
  const initialState = {
    client: null,
    uid: null,
    accessToken: null,
    isAuthenticated: false,
    loading: false,
  };

  const login = async (formData) => {
    try {
      setLoading();
      const res = await axios.post(
        "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in",
        formData
      );
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.headers,
      });
    } catch (e) {
      dispatch({
        type: LOGIN_FAIL,
      });
    }
  };

  const setLoading = () => dispatch({ type: SET_LOADING });

  const [state, dispatch] = useReducer(authReducer, initialState);

  return (
    <AuthContext.Provider
      value={{
        login,
        isAuthenticated: state.isAuthenticated,
        loading: state.loading,
        client: state.client,
        uid: state.uid,
        accessToken: state.accessToken,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
